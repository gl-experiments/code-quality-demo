# code-quality-demo

- If you commit on the `main` branch, the code quality artifact is empty.
- If you commit on a MR, a code quality artifact is generated with quality findings.
